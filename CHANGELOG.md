# Changelog

## 5.0.0 - 2025-11-27

### Added

- Installable en tant que package Composer

### Changed

- spip/spip#6043 utilisation de la méthode de chargement `initjs`
- Utiliser `#LAYOUT_PRIVE` plutôt que `#LARGEUR_ECRAN`
- #4958 Appel à la globale `$formats_logos` remplacée par `_image_extensions_acceptees_en_entree()`
- Externalisation de la lib [james-heinrich/getid3](https://packagist.org/packages/james-heinrich/getid3)
- Appel de l'archiviste via `use Spip\Archiver\SpipArchiver`
- Compatible SPIP 5.0.0-dev

### Fixed

- spip/spip#5460 Utiliser les propriétés logiques dans la CSS de l'espace privé
- #5000 Ne plus réduire implicitement (dans certains cas) l’image retournée par `#LOGO_DOCUMENT`
- !5008 Corriger la duplication (plugin Duplicator par exemple) de logo si le dossier `tmp/upload` n'existe pas
- #4999 Affichage du sélecteur de rôles de documents (avec le plugin en question)
- !5009 Affichage des aperçus dans les modèles emb
- !5010 Correction chemin des plugins mediaelements
- !5010 Pas de fallback Flash

### Removed

- #4935 Gestion historique du portfolio. Suppression de `_COMPORTEMENT_HISTORIQUE_PORTFOLIO`, `_COMPORTEMENT_HISTORIQUE_IMG_DOC_EMB`
& `affiche_bouton_mode_image_portfolio()` et squelettes liés

