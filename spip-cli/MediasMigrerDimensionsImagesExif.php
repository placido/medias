<?php

use Spip\Cli\Console\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MediasMigrerDimensionsImagesExif extends Command
{
	protected function configure(): void {
		$this
			->setName('medias:migrer:exif')
			->setDescription('Inverser les hauteur et largeur des images en mode portrait porteuses d\'un EXIF d\'orientation.')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		spip_timer();

		$erreurs = [];

		if (function_exists('exif_read_data')) {
			include_spip('base/abstract_sql');
			include_spip('inc/exif');

			$this->io->title('Migration des largeur et hauteur des images en mode portrait avec un EXIF d\'orientation');

			// On désactive les revisions… (cf. logo_migrer_en_base())
			$liste_objets_versionnes = $GLOBALS['meta']['objets_versions'] ?? '';
			unset($GLOBALS['meta']['objets_versions']);
			// …et le signalement des editions (cf. logo_migrer_en_base()).
			$articles_modif = $GLOBALS['meta']['articles_modif'] ?? '';
			$GLOBALS['meta']['articles_modif'] = 'non';

			// On récupère toutes les images à vérifier (seuls les formats .jpg et .tiff).
			$images = sql_allfetsel(
				[
					'id_document',
					'fichier',
					'largeur',
					'hauteur',
				],
				'spip_documents',
				sql_in('extension', ['jpg', 'tiff'])
			);

			$this->io->info('Il y a ' . count($images) . ' images à vérifier.');

			foreach ($this->io->progressIterate($images) as $image) {
				$exif = false;
				// Si l'on a bien un EXIF d'orientation, on procède à la mise à jour des largeur et hauteur si l'image est en mode portrait.
				if (file_exists(_DIR_IMG . $image['fichier'])) {
					$exif = @exif_read_data(_DIR_IMG . $image['fichier']);
				}
				if (
					$exif
					&& isset($exif['Orientation'])
					&& exif_determiner_si_portrait($exif['Orientation'])
				) {
					// On ne peut se contenter d'inverser les dimensions déjà enregistrées (au cas où la migration serait rejouée).
					[$largeur, $hauteur] = spip_getimagesize(_DIR_IMG . $image['fichier']);

					// Si les dimensions retournées sont différentes de celles enregistrées en base, on déclenche la mise à jour.
					if (
						$largeur !== $image['largeur']
						|| $hauteur !== $image['hauteur']
					) {
						$resultat = sql_updateq(
							'spip_documents',
							[
								'largeur' => $largeur,
								'hauteur' => $hauteur,
							],
							'id_document = ' . $image['id_document'],
						);

						$resultat ?: $erreurs['ids'][] = $image['id_document'];
					}
				}
			}

			// Réactiver les révisions…
			if ($liste_objets_versionnes) {
				$GLOBALS['meta']['objets_versions'] = $liste_objets_versionnes;
			}
			// …et le signalement des éditions.
			$GLOBALS['meta']['articles_modif'] = $articles_modif;

			effacer_meta('drapeau_edition');
		} else {
			$erreurs['exif'] = 'L\'extension EXIF ne semble pas être disponible ; il n\'est donc pas possible de procéder.';
		}

		if (!empty($erreurs)) {
			if (!empty($erreurs['exif'])) {
				$this->io->error($erreurs['exif']);
			}

			if (!empty($erreurs['ids'])) {
				$this->io->error('Les documents suivants n\'ont pas pu être correctement sauvegardés :');
				$this->io->listing($erreurs['ids']);
			}
		} else {
			$this->io->success('La migration des images s\'est correctement déroulée (durée : ' . spip_timer() . ' ).');
		}

		return self::SUCCESS;
	}
}
