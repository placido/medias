function medias_editbox(_root) {
	const root = _root || document;
	for (const btn of [...root.querySelectorAll(':where(.formulaire_editer_logo, #documents_joints, #portfolios) a.editbox:not(.nobox)')]) {
		btn.removeAttribute('onclick');
		btn.classList.add('nobox');
		btn.addEventListener("click", (e) => {
			e.preventDefault();
			const casedoc = btn.closest('div.item') || '';
			if (casedoc && !!jQuery.modalboxload) {
				animateLoading(casedoc);
				jQuery.modalboxload(parametre_url(parametre_url(btn.getAttribute('href'),'popin','oui'),'var_zajax','contenu'),{
					onClose: () => {ajaxReload(casedoc)}
				});
			}
		});	
	}
}
medias_editbox();
onAjaxLoad(medias_editbox);