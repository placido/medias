<?php

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

if (!defined('_BOUTON_MODE_IMAGE')) {
	define('_BOUTON_MODE_IMAGE', true);
}

include_spip('inc/documents'); // pour la fonction affiche_raccourci_doc
function medias_raccourcis_doc(
	$id_document,
	$titre,
	$descriptif,
	$inclus,
	$largeur,
	$hauteur,
	$mode,
	$vu,
	$media = null
) {
	$raccourci = '';
	$doc = 'doc';

	// Affichage du raccourci <doc...> correspondant
	$raccourci = medias_raccourcis_doc_groupe($doc, $id_document);

	return "<div class='raccourcis'>" . $raccourci . '</div>';
}


function medias_raccourcis_doc_groupe($doc, $id_document): string {
	$raccourci =
		affiche_raccourci_doc($doc, $id_document, '')
		. affiche_raccourci_doc($doc, $id_document, 'left', true)
		. affiche_raccourci_doc($doc, $id_document, 'center', true)
		. affiche_raccourci_doc($doc, $id_document, 'right', true);
	return "<div class='groupe-btns'>$raccourci</div>";
}
